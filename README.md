# cron-list
A bitbucket-hosted script to list all current crons on linux

## Installation
```
#Download the current command
curl https://bitbucket.org/lucabartoli/cron-list/raw/master/crons.sh --output crons.sh
#Apply correct permissions
chmod +x crons.sh
#Move into executable folder
mv crons.sh /usr/local/bin/crons
```

## Usage
Just type `crons` in shell

## Credits
This command has been written originally by [Benjamin W.](https://stackoverflow.com/users/3266847/benjamin-w) on [this Stackoverflow Question](https://stackoverflow.com/a/137173)

## License
The script is released without any warranty. Use it at your own risk.
You're free to redistribute, copy and modify this script.

